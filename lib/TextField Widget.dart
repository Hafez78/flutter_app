import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


void main() {
  runApp(new MaterialApp(home: new Application()));
}

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => new _ApplicationState();
}

class _ApplicationState extends State<Application> {

  String ptext = '';

  Widget build(BuildContext context) {
    return new MaterialApp(
        title: "Texte",
        home: new Scaffold(
        appBar: new AppBar(
        backgroundColor: Colors.green,
          title : new Text("Texte"),
    ),
            body: new Column(
              children: <Widget>[
                new TextField(
                    onChanged: (String tval) {
                      setState(() {
                        ptext = tval;
                      });
                    }),
                new Text(ptext)
              ],
            )
        ),

    );
  }
}










