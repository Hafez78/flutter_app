import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(home: new Application()));
}

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => new _ApplicationState();
}

class _ApplicationState extends State<Application> {

  void dialog(){
    showDialog(
        builder: (context) => new AlertDialog(
          title: new Text('Avertissement'),
          content: new Text('Vous mourrez si vous appuyez sur le bouton de fermeture'),
          actions: <Widget>[
            new IconButton(icon: new Icon(Icons.close), onPressed: (){Navigator.pop(context);})
          ],
        ), context: context
    );
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new RaisedButton(
          onPressed: (){dialog();},
          child: new Text('Activer AlertDialog'),
        ),
      ) ,
    );
  }
}