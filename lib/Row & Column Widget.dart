import 'package:flutter/material.dart';

void main() => runApp(new Application());

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Container',
      home: new Scaffold(
          body: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text('c\'est'),
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text('c\'est'),
                  new Text('un'),
                  new Text('jeu'),
                ],
              ),
              new Text('jeu'),
            ],
          )
      ),
    );
  }
}