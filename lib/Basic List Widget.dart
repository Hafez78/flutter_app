import 'package:flutter/material.dart';


void main() {
  runApp(new MaterialApp(home: new Application()));
}

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => new _ApplicationState();
}

class _ApplicationState extends State<Application> {

  Widget build(BuildContext context) {
    return new Scaffold(
        body: new ListView(
          children: <Widget>[
            new ListTile(
              title: new Text('Hafez Elassad'),
              isThreeLine: true,
              subtitle: new Text('Etudiant à l\'ISET'),
              trailing: new Icon(Icons.arrow_forward),
            ),
            new ListTile(
              title: new Text('Departement'),
              isThreeLine: true,
              subtitle: new Text('Informatique'),
              trailing: new Icon(Icons.arrow_forward),
            ),
            new ListTile(
              title: new Text('Niveau'),
              isThreeLine: true,
              subtitle: new Text('DSI 2'),
              trailing: new Icon(Icons.arrow_forward),
            ),
          ],
        )
    );
  }
}










