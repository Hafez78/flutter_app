import 'package:flutter/material.dart';


void main() => runApp(new Application());

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => new _ApplicationState();
}

class _ApplicationState extends State<Application> with SingleTickerProviderStateMixin {

  late TabController controller;
  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  new MaterialApp(
        title: "Application",
        home: new Scaffold(
          appBar: new AppBar(title: new Text("MY EL2S"),
            bottom: new TabBar(
                controller: controller,
                tabs:[
                  new Tab(icon: new Icon(Icons.home) ,),
                  new Tab(icon: new Icon(Icons.supervisor_account),),
                  new Tab(icon: new Icon(Icons.close),),
                ]),
            backgroundColor: Colors.deepOrange,
          ),
          bottomNavigationBar: new Material(
            color: Colors.deepOrange,
            child: new TabBar(
                controller: controller,
                tabs:[
                  new Tab(icon: new Icon(Icons.home) ,),
                  new Tab(icon: new Icon(Icons.supervisor_account),),
                  new Tab(icon: new Icon(Icons.close),),
                ]),
          ),

          body:  new TabBarView(
            controller: controller,
            children: <Widget>[
              new Center(child:new Text("bienvenue à la maison") ,),
              new Center(child:new Text("bienvenue dans le compte utilisateur") ,),
              new Center(child:new Text("bienvenue à la fermeture") ,),
            ],
          ),
        )
    );
  }
}



